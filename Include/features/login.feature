#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

Feature: Login feature
  I want to use this template for my feature file

  Scenario Outline: Login successfully
    Given I open application
    When I go to login page
    When I field <email> and <password>
    Then I click button login

    Examples: 
      | email 					| password  |
      | test@email.com 	| test |
  
  #Scenario: Login unsuccessfully
    #Given I open application
    #When I go to login page
    #Then I see invalid login message