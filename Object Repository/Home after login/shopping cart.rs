<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>shopping cart</name>
   <tag></tag>
   <elementGuidId>a7844236-3020-4525-bce2-755b0b799a26</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//div/button)[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath1</name>
      <type>Main</type>
      <value>//i[@class='v-icon notranslate fas fa-shopping-cart theme--dark']</value>
      <webElementGuid>2acd78f1-c049-40bc-b297-713a753ce394</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div/button)[1]</value>
      <webElementGuid>94f01f61-758e-45b6-96ee-1f0e32ba5668</webElementGuid>
   </webElementProperties>
</WebElementEntity>
