<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button logout</name>
   <tag></tag>
   <elementGuidId>129e7296-4090-422b-9f1c-33e2c3ef3cb5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(text(),'Logout')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[contains(text(),'Logout')]</value>
      <webElementGuid>c5446b36-8adf-444d-b277-32f7c04b7a88</webElementGuid>
   </webElementProperties>
</WebElementEntity>
