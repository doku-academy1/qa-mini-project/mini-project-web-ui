<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button user</name>
   <tag></tag>
   <elementGuidId>14a5baa0-f7a4-4892-83e6-99c66a9c304a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//i[@class='v-icon notranslate fas fa-user theme--dark']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//i[@class='v-icon notranslate fas fa-user theme--dark']</value>
      <webElementGuid>4d5a91c2-7d64-4084-9fb7-503b2ed30e4b</webElementGuid>
   </webElementProperties>
</WebElementEntity>
