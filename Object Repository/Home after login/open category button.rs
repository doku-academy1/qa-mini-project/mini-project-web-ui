<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>open category button</name>
   <tag></tag>
   <elementGuidId>645d3186-fdcb-4a1c-98e1-b4848278b71d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='v-input__icon v-input__icon--append']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='v-input__icon v-input__icon--append']</value>
      <webElementGuid>a8dba016-b969-46b2-8782-ab09b1f0d051</webElementGuid>
   </webElementProperties>
</WebElementEntity>
