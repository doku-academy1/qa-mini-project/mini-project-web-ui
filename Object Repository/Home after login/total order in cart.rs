<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>total order in cart</name>
   <tag></tag>
   <elementGuidId>9eea93b9-ebd6-4fb2-99b2-8d5682c983c2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()!=0 and @role='status']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()!=0 and @role='status']</value>
      <webElementGuid>7199d6bf-ba32-497d-9c75-b2227862d29c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
