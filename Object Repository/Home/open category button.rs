<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>open category button</name>
   <tag></tag>
   <elementGuidId>3736abdb-7475-47e3-8894-69fcbaa847ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//i[@class='v-icon notranslate fas fa-caret-down theme--light']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//i[@class='v-icon notranslate fas fa-caret-down theme--light']</value>
      <webElementGuid>1281b50b-6def-4cdf-a5f4-4d505546e782</webElementGuid>
   </webElementProperties>
</WebElementEntity>
