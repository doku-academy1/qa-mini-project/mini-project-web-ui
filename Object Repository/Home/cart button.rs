<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>cart button</name>
   <tag></tag>
   <elementGuidId>bfa1feb8-ca36-4f6d-b82b-e2e1dc11c7ad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[@class='v-badge__wrapper']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[@class='v-badge__wrapper']</value>
      <webElementGuid>c6f41609-7c14-468d-a9d0-047bec163f02</webElementGuid>
   </webElementProperties>
</WebElementEntity>
