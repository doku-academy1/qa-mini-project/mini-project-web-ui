<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>logout or login page</name>
   <tag></tag>
   <elementGuidId>98c45cd8-3160-41fc-8cca-2b91efe5d05f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//i[@class='v-icon notranslate fas fa-sign-in theme--dark']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//i[@class='v-icon notranslate fas fa-sign-in theme--dark']</value>
      <webElementGuid>af7d7b3d-5f7e-4d70-b616-f4488e60376b</webElementGuid>
   </webElementProperties>
</WebElementEntity>
