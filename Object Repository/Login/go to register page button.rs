<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>go to register page button</name>
   <tag></tag>
   <elementGuidId>afe423e1-62bc-4330-86d8-3253e598c118</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[text()='Register']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Register']</value>
      <webElementGuid>7ef57975-7e11-45c3-8bef-f17f08a9403b</webElementGuid>
   </webElementProperties>
</WebElementEntity>
