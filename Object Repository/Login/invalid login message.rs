<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>invalid login message</name>
   <tag></tag>
   <elementGuidId>aedb6e56-894f-46f7-9672-079050a5f984</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@role='alert']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@role='alert']</value>
      <webElementGuid>b0ac2b11-01a5-462f-9ec8-7422cf936728</webElementGuid>
   </webElementProperties>
</WebElementEntity>
