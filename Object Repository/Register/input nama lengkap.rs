<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input nama lengkap</name>
   <tag></tag>
   <elementGuidId>57899cdd-0832-4ea7-bc54-d43c99641a85</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[text()='Nama Lengkap']/following-sibling::input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath1</name>
      <type>Main</type>
      <value>(//input[@type='text'])[1]</value>
      <webElementGuid>2418129a-5b30-495f-9f94-396e02efc8ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Nama Lengkap']/following-sibling::input</value>
      <webElementGuid>992e2094-a6a7-4b7e-8cdb-9e5f84c326cf</webElementGuid>
   </webElementProperties>
</WebElementEntity>
