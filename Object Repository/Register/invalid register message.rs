<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>invalid register message</name>
   <tag></tag>
   <elementGuidId>10cadab6-93ae-43b4-b8ee-9bc17239d0b6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@role='alert']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@role='alert']</value>
      <webElementGuid>1db4de21-7926-4d5b-bd4c-a07df74802f1</webElementGuid>
   </webElementProperties>
</WebElementEntity>
