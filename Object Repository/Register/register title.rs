<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>register title</name>
   <tag></tag>
   <elementGuidId>91835b7f-c3cc-4e08-a934-7c7b8a14abed</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='v-card__title']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='v-card__title']</value>
      <webElementGuid>4e8180ec-a802-4098-8963-a532970db5ad</webElementGuid>
   </webElementProperties>
</WebElementEntity>
