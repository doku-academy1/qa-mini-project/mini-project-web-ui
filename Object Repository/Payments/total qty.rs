<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>total qty</name>
   <tag></tag>
   <elementGuidId>fe26be72-35a6-4c49-b410-f23f3d374700</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[@id='label-total-quantity']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[@id='label-total-quantity']</value>
      <webElementGuid>16be1be4-6210-4afd-8dd3-647f4707dbb2</webElementGuid>
   </webElementProperties>
</WebElementEntity>
